package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameStartingWithIgnoreCase(String lastName, PageRequest pageable);


    @Query(" SELECT u " +
            " FROM User u " +
            " JOIN u.office o " +
            " WHERE o.city = :city ")
    List<User> findByCity(@Param("city") String city, Sort sort);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query(" SELECT u " +
            " FROM User u " +
            " JOIN u.office o " +
            " JOIN u.team t " +
            " WHERE o.city = :city " +
            " AND t.room = :room ")
    List<User> findByRoomAndCity(@Param("city") String city, @Param("room") String room, Sort sort);

    @Modifying
    @Query(" DELETE FROM User u " +
            " WHERE u.experience < :experience")
    int deletedUsersLessThanExperience(@Param("experience") int experience);
}
