package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {


    @Modifying
    @Transactional
    @Query(" DELETE FROM Role r " +
            " WHERE r.code = :code " +
            " AND r.users.size = 0 ")
    void deletedByCode(@Param("code") String code);
}
