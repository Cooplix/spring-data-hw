package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(" SELECT o FROM Office o " +
            " INNER JOIN o.users u " +
            " INNER JOIN  u.team t " +
            " INNER JOIN t.project p " +
            " INNER JOIN  t.technology tech " +
            " WHERE tech = :technology " +
            " GROUP BY o.id ")
    List<Office> getByTechnology(@Param("technology") String technology);

    Office findOneByAddress(String address);


    @Modifying
    @Transactional
    @Query(nativeQuery=true, value = " UPDATE offices " +
            " SET address = :address " +
            " FROM ( " +
            "         SELECT o.id FROM offices AS o " +
            "             INNER JOIN users u ON o.id = u.office_id " +
            "             INNER JOIN teams t ON u.team_id = t.id " +
            "             INNER JOIN projects p ON t.project_id = p.id " +
            "             WHERE o.address = :old_address " +
            "         ) AS o " +
            " WHERE o.id = offices.id")
    void updateOfficeAddress(@Param("old_address") String oldAddress, @Param("address") String newAddress);
}
