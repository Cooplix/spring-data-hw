package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findByName(String name);

    int countByTechnologyName(String name);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = " UPDATE teams AS tt " +
                    " SET name = tt.name || '_' || p.name || '_' || t.name " +
                    " FROM technologies as t, projects as p " +
                    " WHERE tt.project_id = p.id " +
                    " AND t.id = tt.technology_id " +
                    " AND tt.name = :hipsters ")
    void normalizeName(@Param("hipsters") String hipsters);

}
