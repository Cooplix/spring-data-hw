package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query(nativeQuery = true, value =
    " SELECT p.*, count(u.id) AS countUser " +
            " FROM projects p " +
            " INNER JOIN teams t ON t.project_id = p.id" +
            " INNER JOIN technologies tl ON t.technology_id = tl.id" +
            " INNER JOIN users u ON u.team_id = t.id " +
            " WHERE tl.name = :technology " +
            " GROUP BY p.id " +
            " ORDER BY countUser DESC")
    List<Project> findByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query(nativeQuery = true, value =
    " SELECT p.*, " +
            " count(t.id) AS countTeam, " +
            " count(u.id) AS countUser " +
            " FROM Projects p " +
            "   INNER JOIN teams t ON p.id = t.project_id " +
            "   INNER JOIN users u ON t.id = u.team_id " +
            " GROUP BY p.id " +
            "ORDER BY countTeam DESC, countUser DESC, p.name DESC  limit 1")
    List<Project> findTheBiggest();



    @Query(value = "select count(t.proj_id) " +
            "from (select proj.id as proj_id from projects proj " +
            "    inner join teams t on proj.id = t.project_id" +
            "    inner join users u on t.id = u.team_id" +
            "    inner join user2role u2r on u.id = u2r.user_id" +
            "    inner join roles r on u2r.role_id = r.id" +
            "    where r.name = :role" +
            "    group by proj.id" +
            "    ) t", nativeQuery = true)
    int countByUsersRoles(@Param("role") String role);

}